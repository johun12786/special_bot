import time
from flask import Flask, jsonify, render_template, request
from flask_cors import CORS, cross_origin
from linebot.models import *
from linebot.models.template import *
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import LineBotApiError
from function.func import *
from random import choice


app = Flask(__name__)
cors = CORS(app)
channel_accesstoken = "6IPUv1DEh1TU77dHRBrXaovwXEx9XD3XuOmphalk+DER4pCKxb6cBgLHPy4rylU6c/Wxqld9ueD4+zIXwuv4wljF8L6DIJ9GqTzpvBki2z5InHtgDOBvO3gnn7o8jfZWKAjRnwqc5gwMNvmiV1V5dQdB04t89/1O/w1cDnyilFU="
channel_secret = "9e8ff2e26f7edf4b9d4931ecb7077f72"
line_bot_api = LineBotApi(channel_accesstoken)
handler = WebhookHandler(channel_secret)


@app.route("/", methods=["GET"])
@cross_origin()
def index():
    return render_template("index.html")


@app.route("/webhook", methods=["GET", "POST"])
@cross_origin()
def hooking():
    signature = request.headers["x-line-signature"]
    body = request.get_data(as_text=True)
    handler.handle(body, signature)
    return "THIS IS WEBHOOK", 200


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    app.logger.info("Event : " + str(event))
    app.logger.info(event.message.text)
    item_reply = set_reply(event.message.text)
    if item_reply != None:
        line_bot_api.reply_message(event.reply_token, item_reply)
    elif item_reply == None and set_postback(event.message.text) == None:
        line_bot_api.reply_message(event.reply_token, [TextSendMessage(text="เตยพูดไรอะ Booo! ไม่เข้าใจ"),StickerSendMessage(package_id="6325", sticker_id="10979922")])


@handler.add(PostbackEvent)
def handle_postback(event):
    print("Postback : " + str(event))
    food_reply = ["ขอคิดแปบน้าา", "อะไรดีน้าา", "อันนี้น่ากินน"]
    music_reply = ["ขอคิดแปบน้าา", "เพลงนี้ๆๆ", "ลองฟังเพลงนี้ดู", "เพลงนี้เพราะะ"]
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(
            text=choice(music_reply if event.postback.data == "อยากฟังเพลงง" else food_reply),
        ),
    )
    time.sleep(1)
    item_reply = set_postback(event.postback.data)
    if item_reply != None:
        line_bot_api.push_message(event.source.user_id, item_reply)


if __name__ == "__main__":  # main func
    app.run(host="0.0.0.0", debug=True)

from linebot.models import *
import mysql.connector
import json
from random import choice

mydb = mysql.connector.connect(host="157.230.37.137" , user="natawat12786", password="Farm#12786", database="line_bot")


def commit_query(cmd, val=None):
    mycursor = mydb.cursor()
    mycursor.execute(cmd, val)
    mydb.commit()


def get_query(cmd, val=None):
    mycursor = mydb.cursor()
    mycursor.execute(cmd, val)
    myresult = mycursor.fetchall()
    row_headers = [x[0] for x in mycursor.description]  # this will extract row headers
    json_data = []
    for result in myresult:
        json_data.append(dict(zip(row_headers, result)))
    return json_data


def set_reply(message):
    match message:
        case "หิวแน้วว":
            return (
                TextSendMessage(
                    text="อยากกินอะไร เดี๋ยวเชฟจะเลือกให้",
                    quick_reply=QuickReply(
                        items=[
                            {
                                "type": "action",
                                "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/dessert.png",
                                "action": {"type": "postback", "label": "ขนม", "data": "ขนม", "text": "อยากกินขนมอะ"},
                            },
                            {
                                "type": "action",
                                "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/drink_boba.png",
                                "action": {
                                    "type": "postback",
                                    "label": "เครื่องดื่ม",
                                    "data": "น้ำ",
                                    "text": "อยากกินน้ำอะ",
                                },
                            },
                            {
                                "type": "action",
                                "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/7-11.png",
                                "action": {
                                    "type": "postback",
                                    "label": "7-11",
                                    "data": "7-11",
                                    "text": "อยากกินขนมในเซเว่นอะ",
                                },
                            },
                            {
                                "type": "action",
                                "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/yum.png",
                                "action": {"type": "postback", "label": "ยำ", "data": "ยำ", "text": "อยากกินยำอะ"},
                            },
                            {
                                "type": "action",
                                "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/rice.png",
                                "action": {
                                    "type": "postback",
                                    "label": "ข้าว",
                                    "data": "ข้าว",
                                    "text": "อยากกินข้าวอะ",
                                },
                            },
                            {
                                "type": "action",
                                "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/noodle.png",
                                "action": {
                                    "type": "postback",
                                    "label": "เส้น",
                                    "data": "เส้น",
                                    "text": "อยากกินอะไรเป็นเส้นอะ",
                                },
                            },
                        ]
                    ),
                ),
            )
        case "อยู่ด้วยกันหน่อยย":
            return TextSendMessage(
                text="วันนี้เป็นยังไงบ้าง",
                quick_reply=QuickReply(
                    items=[
                        {
                            "type": "action",
                            "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/smile_new.png",
                            "action": {"type": "message", "label": "ชิวชิ๊ววว", "text": "ชิวชิ๊ววว"},
                        },
                        {
                            "type": "action",
                            "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/precry.png",
                            "action": {
                                "type": "message",
                                "label": "แงง ขอกำลังใจหน่อยย",
                                "text": "แงง ขอกำลังใจหน่อยย",
                            },
                        },
                    ]
                ),
            )
        case "เบื่อจางงง":
            return (
                TextSendMessage(
                    text="วันว่างๆแบบนี้ทำไรดี",
                    quick_reply=QuickReply(
                        items=[
                            {
                                "type": "action",
                                "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/music.png",
                                "action": {
                                    "type": "postback",
                                    "label": "อยากฟังเพลงง",
                                    "data": "อยากฟังเพลงง",
                                    "text": "อยากฟังเพลงง",
                                },
                            },
                            {
                                "type": "action",
                                "imageUrl": "https://line-special-bot.s3.ap-southeast-1.amazonaws.com/precry.png",
                                "action": {
                                    "type": "message",
                                    "label": "เหงาา คุยด้วยหน่อยย",
                                    "text": "เหงาา คุยด้วยหน่อยย",
                                },
                            },
                        ]
                    ),
                ),
            )
        case "ชิวชิ๊ววว":
            return (
                TextSendMessage(
                    text="เก่งมากก อย่าลืมกินข้าวด้วยน้าา มีคนเปงห่วงง",
                ),
            )
        case "แงง ขอกำลังใจหน่อยย":
            return (
                TextSendMessage(
                    text="หนายย เป็นอะไร บอกหมามาซิ",
                    quick_reply=QuickReply(
                        items=[
                            {
                                "type": "action",
                                "action": {
                                    "type": "message",
                                    "label": "ของอแงหน่อยย",
                                    "text": "ของอแงหน่อยย",
                                },
                            }
                        ]
                    ),
                ),
            )
        case "ของอแงหน่อยย":
            return (
                TextSendMessage(
                    text="มาาๆ ",
                    quick_reply=QuickReply(
                        items=[
                            {
                                "type": "action",
                                "action": {
                                    "type": "uri",
                                    "label": "หมาาาาาาาาา",
                                    "uri": "https://line.me/ti/p/~farm12786",
                                },
                            }
                        ]
                    ),
                ),
            )
        case "เหงาา คุยด้วยหน่อยย":
            return (
                TextSendMessage(
                    text="มาๆ ทักมาเร้ววว",
                    quick_reply=QuickReply(
                        items=[
                            {
                                "type": "action",
                                "action": {
                                    "type": "uri",
                                    "label": "หมาาาาาาาาา",
                                    "uri": "https://line.me/ti/p/~farm12786",
                                },
                            }
                        ]
                    ),
                ),
            )
        case "อยากกินขนมอะ":
            return True
        case "อยากกินน้ำอะ":
            return True
        case "อยากกินขนมในเซเว่นอะ":
            return True
        case "อยากกินยำอะ":
            return True
        case "อยากกินข้าวอะ":
            return True
        case "อยากกินอะไรเป็นเส้นอะ":
            return True
        case "ไม่เอาอะ มีอะไรน่ากินอีก":
            return True
        case "มีเพลงไรน่าฟังอีก":
            return True
        case _:
            return None
    


def set_postback(postback_data):
    match postback_data:
        case "ขนม":
            snack_list = get_query(f"SELECT food_name FROM food WHERE category = {'1'}")
            choose_snack = choice(snack_list)
            return TextSendMessage(
                text=choose_snack["food_name"] + " !",
                quick_reply=QuickReply(
                    items=[
                        {
                            "type": "action",
                            "action": {
                                "type": "postback",
                                "label": "ไม่เอาอะ มีอะไรน่ากินอีก",
                                "data": "ขนม",
                                "text": "ไม่เอาอะ มีอะไรน่ากินอีก",
                            },
                        }
                    ]
                ),
            )
        case "น้ำ":
            drink_list = get_query(f"SELECT food_name FROM food WHERE category = {'2'}")
            choose_drink = choice(drink_list)
            return TextSendMessage(
                text=choose_drink["food_name"] + " !",
                quick_reply=QuickReply(
                    items=[
                        {
                            "type": "action",
                            "action": {
                                "type": "postback",
                                "label": "ไม่เอาอะ มีอะไรน่ากินอีก",
                                "data": "น้ำ",
                                "text": "ไม่เอาอะ มีอะไรน่ากินอีก",
                            },
                        }
                    ]
                ),
            )
        case "7-11":
            seven_list = get_query(f"SELECT food_name FROM food WHERE category = {'3'}")
            choose_seven = choice(seven_list)
            return TextSendMessage(
                text=choose_seven["food_name"] + " !",
                quick_reply=QuickReply(
                    items=[
                        {
                            "type": "action",
                            "action": {
                                "type": "postback",
                                "label": "ไม่เอาอะ มีอะไรน่ากินอีก",
                                "data": "7-11",
                                "text": "ไม่เอาอะ มีอะไรน่ากินอีก",
                            },
                        }
                    ]
                ),
            )
        case "ยำ":
            yum_list = get_query(f"SELECT food_name FROM food WHERE category = {'4'}")
            choose_yum = choice(yum_list)
            return TextSendMessage(
                text=choose_yum["food_name"] + " !",
                quick_reply=QuickReply(
                    items=[
                        {
                            "type": "action",
                            "action": {
                                "type": "postback",
                                "label": "ไม่เอาอะ มีอะไรน่ากินอีก",
                                "data": "ยำ",
                                "text": "ไม่เอาอะ มีอะไรน่ากินอีก",
                            },
                        }
                    ]
                ),
            )
        case "ข้าว":
            rice_list = get_query(f"SELECT food_name FROM food WHERE category = {'5'}")
            choose_rice = choice(rice_list)
            return TextSendMessage(
                text=choose_rice["food_name"] + " !",
                quick_reply=QuickReply(
                    items=[
                        {
                            "type": "action",
                            "action": {
                                "type": "postback",
                                "label": "ไม่เอาอะ มีอะไรน่ากินอีก",
                                "data": "ข้าว",
                                "text": "ไม่เอาอะ มีอะไรน่ากินอีก",
                            },
                        }
                    ]
                ),
            )
        case "เส้น":
            noodle_list = get_query(f"SELECT food_name FROM food WHERE category = {'6'}")
            choose_noodle = choice(noodle_list)
            return TextSendMessage(
                text=choose_noodle["food_name"] + " !",
                quick_reply=QuickReply(
                    items=[
                        {
                            "type": "action",
                            "action": {
                                "type": "postback",
                                "label": "ไม่เอาอะ มีอะไรน่ากินอีก",
                                "data": "เส้น",
                                "text": "ไม่เอาอะ มีอะไรน่ากินอีก",
                            },
                        }
                    ]
                ),
            )
        case "อยากฟังเพลงง":
            music_list = get_query(f"SELECT music_name, artist FROM music")
            choose_music = choice(music_list)
            return TextSendMessage(
                text=choose_music["music_name"] + " - " + choose_music["artist"],
                quick_reply=QuickReply(
                    items=[
                        {
                            "type": "action",
                            "action": {
                                "type": "postback",
                                "label": "มีเพลงไรน่าฟังอีก",
                                "data": "อยากฟังเพลงง",
                                "text": "มีเพลงไรน่าฟังอีก",
                            },
                        }
                    ]
                ),
            )
        case _:
            return None


def random():
    return True

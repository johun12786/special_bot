FROM python:3.10-slim-buster

WORKDIR /app
ENV FLASK_APP=app.py
ENV FLASK_ENV=development

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN pip install gunicorn

COPY . .

# CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]
# CMD [ "flask", "run", "--host=0.0.0.0"]
CMD ["gunicorn", "--access-logfile", "-", "--error-logfile", "-", "-b", "0.0.0.0:5000", "app:app"]
